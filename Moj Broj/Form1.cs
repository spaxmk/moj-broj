﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Moj_Broj
{
    public partial class Form1 : Form
            {
                double NumberOne = 0;
                double NumberTwo = 0;
                double NumberThree = 0;
                double NumberFour = 0;
                double NumberFive = 0;
                double NumberSix = 0;

                string value = "";
                int trazenibroj = 0;
                double dobijenibroj = 0;
                string brojbroj = "";
                int josjedanbroj = 0;

                int vremezaodbrojavanje = 90;

                bool Done = false;
                bool proverabr = false;
                bool proverazagrade = false;

                string CurValue = "";
        public Form1()
        {
            InitializeComponent();
            LbResenje.Visible = false;
        }

  
        private void BtnStart_Click(object sender, EventArgs e)
        {
            TimerRandom.Start();

            RandomingNumbers();
            BtnStart.Enabled = false;
            BtnStop.Enabled = true;
            LbResenje.Enabled = true;

        }
        
        private void RandomingNumbers()
        {
            TimerRandom.Start();
            Random rnd = new Random();
            int valueone = rnd.Next(1, 999);
            TBTrazeni.Text = valueone.ToString();
            
            int valuetwo = rnd.Next(1, 10);
            TBJedan.Text = valuetwo.ToString();
            int valuethree = rnd.Next(1, 10);
            TBDva.Text = valuethree.ToString();
            int valuefour = rnd.Next(1, 10);
            TBTri.Text = valuefour.ToString();
            int valuefive = rnd.Next(1, 10);
            TBCetiri.Text = valuefive.ToString();

            int valuesix = rnd.Next(1, 4);
            if (valuesix == 1)
            {
                TBPet.Text = 10.ToString();
            }
            if (valuesix == 2)
            {
                TBPet.Text = 15.ToString();
            }
            if (valuesix == 3)
            {
                TBPet.Text = 20.ToString();
            }


            int valueseven = rnd.Next(1, 5);
            if (valueseven == 1)
            {
                TBSest.Text = 25.ToString();
            }
            if (valueseven == 2)
            {
                TBSest.Text = 50.ToString();
            }
            if (valueseven == 3)
            {
                TBSest.Text = 75.ToString();
            }
            if (valueseven == 4)
            {
                TBSest.Text = 100.ToString();
            }
    
        }

        private void TimerRandom_Tick(object sender, EventArgs e)
        {
            TimerRandom.Stop();
            RandomingNumbers();
            
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
        BtnPlus.Enabled = true;
        BtnMinus.Enabled = true;
        BtnPuta.Enabled = true;
        BtnPodeljeno.Enabled = true;
        BtnZagrada2.Enabled = true;
        BtnZagrada1.Enabled = true;
        BtnObrisi.Enabled = true;

        LbResenje.Visible = true;

        TimerRandom.Stop();
        BtnStop.Enabled = false;
        BtnPotvrdi.Enabled = true;
        TimerVreme.Start();
        VremeOdbrojavanje();
        trazenibroj = Convert.ToInt32(TBTrazeni.Text);
        }

        private void TimerVreme_Tick(object sender, EventArgs e)
        {
        TimerVreme.Stop();
        value = TextBox1.Text;
        brojbroj = TextBox1.Text;
        TimerVreme.Start();
        }

        private void VremeOdbrojavanje()
        {
            TimerOdbrojavanje.Start();
        }

        private void TimerOdbrojavanje_Tick(object sender, EventArgs e)
        {
        TimerOdbrojavanje.Stop();
        vremezaodbrojavanje = vremezaodbrojavanje - 1;
        LbVreme.Text = vremezaodbrojavanje.ToString();
        TimerOdbrojavanje.Start();
        ProveriTajmer();
        }

        private void ProveriTajmer()
        {
            if (vremezaodbrojavanje == 0)
            {
            LbResenje.Text = "Vreme je isteklo!";
            TimerOdbrojavanje.Stop();
            BtnPotvrdi.Enabled = false;
            BtnStart.Enabled = false;
            BtnStop.Enabled = false;
            BtnObrisi.Enabled = false;
            }
        }

        private void BtnRestart_Click(object sender, EventArgs e)
        {
            Application.Restart();
        } //restart aplikacije

        private void BtnObrisi_Click(object sender, EventArgs e)
        {
            try
            {
                proverabr = false;
                TextBox1.Text = "";
                TBDva.Tag = "";
                TBJedan.Tag = "";
                TBTri.Tag = "";
                TBCetiri.Tag = "";
                TBPet.Tag = "";
                TBSest.Tag = "";
                BtnPotvrdi.Enabled = true;

                CurValue = "";
                value = "";
                LbDigitron.Text = "0";
                NumberOne = 0;
                NumberTwo = 0;
                NumberThree = 0;
                NumberFour = 0;
                NumberFive = 0;
                NumberSix = 0;

                TBJedan.BackColor = Color.White;
                TBDva.BackColor = Color.White;
                TBTri.BackColor = Color.White;
                TBCetiri.BackColor = Color.White;
                TBPet.BackColor = Color.White;
                TBSest.BackColor = Color.White;
            }
               
            catch (Exception)
            {
                
                throw;
            }
        } //brisanje klik

      
        //tbjedan klik
        private void TBJedan_Click(object sender, EventArgs e)
        {
            if ((NumberOne == 0) && (proverabr==false))
            {
            NumberOne = Convert.ToInt32(TBJedan.Text);
            TBJedan.Tag = "1";
                TBJedan.BackColor = Color.DeepSkyBlue;
                AddDigitsToTextBox();
                proverabr = true;
            }
                      
        }
        private void AddDigitsToTextBox()
        {
         TextBox1.Text += NumberOne;
                  
        }
        //tbjedan klik
        
        //tbdva klik
        private void TBDva_Click(object sender, EventArgs e)
        {
            if ((NumberTwo == 0) && (proverabr==false))
            {
            NumberTwo = Convert.ToInt32(TBDva.Text);
            TBDva.Tag = "1";
                TBDva.BackColor = Color.DeepSkyBlue;
                adddigitstotextbox2();
                proverabr = true;
        }

        }    
        private void adddigitstotextbox2()
            {
                TextBox1.Text += NumberTwo;
            }
        //tbdva klik

        //tbtri klik
        private void TBTri_Click(object sender, EventArgs e)
        {
            if ((NumberThree == 0) && (proverabr==false))
            {
                NumberThree = Convert.ToInt32(TBTri.Text);
                TBTri.Tag = "1";
                TBTri.BackColor = Color.DeepSkyBlue;
                addidgis();
                proverabr = true;
            }

        }
        private void addidgis()
        {
            TextBox1.Text += NumberThree;
        }
        //tbtri klik

        //tbcetiri klik
        private void TBCetiri_Click(object sender, EventArgs e)
        {
            if ((NumberFour == 0) && (proverabr==false))
            {
                NumberFour = Convert.ToInt32(TBCetiri.Text);
                TBCetiri.Tag = "1";
                TBCetiri.BackColor = Color.DeepSkyBlue;
                addigiitsasa();
                proverabr = true;
            }
        }
        private void addigiitsasa()
        {
            TextBox1.Text += NumberFour;
        }
        //tbcetiri klik

        //tbpet klik
        private void TBPet_Click(object sender, EventArgs e)
                {
                    if ((NumberFive == 0) && (proverabr==false))
                    {
                        NumberFive = Convert.ToInt32(TBPet.Text);
                        TBPet.Tag = "1";
                        TBPet.BackColor = Color.DeepSkyBlue;
                        addigitalsias();
                        proverabr = true;
                    }
                }
        private void addigitalsias()
                {
                    TextBox1.Text += NumberFive;
                }
        //tbpet klik

        //tbsest klik
        private void TBSest_Click(object sender, EventArgs e)
               {
                   if ((NumberSix == 0) && (proverabr==false))
                   {
                       NumberSix = Convert.ToInt32(TBSest.Text);
                       TBSest.Tag = "1";
                       TBSest.BackColor = Color.DeepSkyBlue;
                       addingdigirssix();
                       proverabr = true;
                   }
               }
        private void addingdigirssix()
               {
                   TextBox1.Text += NumberSix;
               }
        //tbsest klik

        private void BtnPlus_Click(object sender, EventArgs e) //klik na +
        {
            proverabr = false;
            if (TextBox1.Text.EndsWith("+"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("-"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("*"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("/"))
            {
                return;
            }
            else if (string.IsNullOrEmpty(TextBox1.Text))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("+"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("-"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("*"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("/"))
            {
                return;
            }
            else
            {
                TextBox1.Text = TextBox1.Text +  "+";
            }

        }
        private void BtnMinus_Click(object sender, EventArgs e)
        {
            proverabr = false;
            if (TextBox1.Text.EndsWith("+"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("-"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("*"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("/"))
            {
                return;
            }
            else if (string.IsNullOrEmpty(TextBox1.Text))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("+"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("-"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("*"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("/"))
            {
                return;
            }
            else
            {
                TextBox1.Text = TextBox1.Text + "-";
            }

              } // klik na -
        private void BtnPuta_Click(object sender, EventArgs e)
        {
            proverabr = false;
            if (TextBox1.Text.EndsWith("+"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("-"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("*"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("/"))
            {
                return;
            }
            else if (string.IsNullOrEmpty(TextBox1.Text))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("+"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("-"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("*"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("/"))
            {
                return;
            }
            else
            {
                TextBox1.Text = TextBox1.Text + "*";
            }
        } // klik na *
        private void BtnPodeljeno_Click(object sender, EventArgs e)
        {
            proverabr = false;
            if (TextBox1.Text.EndsWith("+"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("-"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("*"))
            {
                return;
            }
            else if (TextBox1.Text.EndsWith("/"))
            {
                return;
            }
            else if (string.IsNullOrEmpty(TextBox1.Text))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("+"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("-"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("*"))
            {
                return;
            }
            else if (TextBox1.Text.StartsWith("/"))
            {
                return;
            }
            else
            {
                TextBox1.Text = TextBox1.Text + "/";
            }
        } // klik na /
        private void BtnZagrada1_Click(object sender, EventArgs e)
        {
            if (proverabr == true) 
            {
                proverazagrade = false;
            }
            else

            TextBox1.Text += "(";
        } // klik na (
        private void Zagrada2_Click(object sender, EventArgs e)
        {
            if (proverabr == false)
            {
                proverazagrade = true;
            }
            else

            TextBox1.Text += ")";
        } // klik na )

        
        private void BtnPotvrdi_Click(object sender, EventArgs e) //klik na potrvrdi
        {
            value = TextBox1.Text;
            TimerVreme.Stop();
            BtnPotvrdi.Enabled = false;
            Done = true;
            CalculateNOW(); 
        }

        private void CalculateNOW()
        {
            try
            {
                double resultTwo;
                dynamic calc = new DataTable();
                resultTwo = Convert.ToDouble(calc.Compute(value, null));
                            dobijenibroj = resultTwo;
             }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
                  ShowingResults();
        }

        private void ShowingResults()
        {
            TimerOdbrojavanje.Stop();
            BtnObrisi.Enabled = false;
            if (trazenibroj == dobijenibroj)
            {
                LbResenje.Text = "";
                LbResenje.Text += "Vas broj je " + dobijenibroj + "." + "Dobili ste tacan broj! Bravo!";
                LbDigitron.Text = Convert.ToString(dobijenibroj);
            }
            else
            {
                LbResenje.Text = "";
                LbResenje.Text += "Vas broj je " + dobijenibroj + "." + " Udaljenost od konacnog broja je: " + (trazenibroj - dobijenibroj) + ".";
                LbDigitron.Text = Convert.ToString(dobijenibroj);
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
              CheckResults();
        }



        private void CheckResults()
        {
            if (TextBox1.Text.EndsWith("+"))
            {
                CurValue = TextBox1.Text;
                CurValue.TrimEnd('+');
            }
            if (TextBox1.Text.EndsWith("-"))
            {
                CurValue = TextBox1.Text;
                CurValue.TrimEnd('-');
            }
            if (TextBox1.Text.EndsWith("*"))
            {
                CurValue = TextBox1.Text;
                CurValue.TrimEnd('*');
            }
            if (TextBox1.Text.EndsWith("/"))
            {
                CurValue = TextBox1.Text;
                CurValue.TrimEnd('/');
            }

            if (String.IsNullOrEmpty(TextBox1.Text))
            {
                return;
            }
            else
            {
                try
                {
                    CurValue = TextBox1.Text;
                    double resultTwo;
                    dynamic calc = new DataTable();
                    resultTwo = Convert.ToDouble(calc.Compute(CurValue, null));
                    dobijenibroj = resultTwo;
                    if (dobijenibroj == trazenibroj)
                    {
                        TimerVreme.Stop();
                        BtnPotvrdi.Enabled = false;
                        Done = true;
                        ShowingResults();
                    }
                    else
                    {
                        LbDigitron.Text = Convert.ToString(dobijenibroj);
                    }
                }
                catch (Exception)
                {

                    //throw;
                }
            }

                    
             
              }

        private void restartujToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        private void aboutMojBorojToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmAbout frmAbout = new frmAbout();
            frmAbout.Show();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            int duzina;
            string str=TextBox1.Text;

            duzina = TextBox1.TextLength;
            if (str!="")
                str=str.Substring(0,duzina-1);
            TextBox1.Text = str;
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void novaIgraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        
    }
               


    }


      