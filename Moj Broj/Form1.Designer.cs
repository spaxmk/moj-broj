﻿namespace Moj_Broj
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.LbDigitron = new System.Windows.Forms.Label();
            this.LbVreme = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Button11 = new System.Windows.Forms.Button();
            this.BtnObrisi = new System.Windows.Forms.Button();
            this.BtnZagrada1 = new System.Windows.Forms.Button();
            this.BtnZagrada2 = new System.Windows.Forms.Button();
            this.BtnMinus = new System.Windows.Forms.Button();
            this.BtnPuta = new System.Windows.Forms.Button();
            this.BtnPodeljeno = new System.Windows.Forms.Button();
            this.BtnPlus = new System.Windows.Forms.Button();
            this.LbResenje = new System.Windows.Forms.Label();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.BtnStop = new System.Windows.Forms.Button();
            this.BtnStart = new System.Windows.Forms.Button();
            this.TBSest = new System.Windows.Forms.TextBox();
            this.TBPet = new System.Windows.Forms.TextBox();
            this.TBDva = new System.Windows.Forms.TextBox();
            this.TBTri = new System.Windows.Forms.TextBox();
            this.TBCetiri = new System.Windows.Forms.TextBox();
            this.TBJedan = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TBTrazeni = new System.Windows.Forms.TextBox();
            this.BtnPotvrdi = new System.Windows.Forms.Button();
            this.TimerRandom = new System.Windows.Forms.Timer(this.components);
            this.TimerVreme = new System.Windows.Forms.Timer(this.components);
            this.TimerOdbrojavanje = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.restartujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaIgraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMojBorojToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button12 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LbDigitron
            // 
            this.LbDigitron.AutoSize = true;
            this.LbDigitron.Font = new System.Drawing.Font("Comic Sans MS", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbDigitron.ForeColor = System.Drawing.Color.Black;
            this.LbDigitron.Location = new System.Drawing.Point(484, 195);
            this.LbDigitron.Name = "LbDigitron";
            this.LbDigitron.Size = new System.Drawing.Size(43, 49);
            this.LbDigitron.TabIndex = 49;
            this.LbDigitron.Text = "0";
            // 
            // LbVreme
            // 
            this.LbVreme.AutoSize = true;
            this.LbVreme.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbVreme.ForeColor = System.Drawing.Color.Red;
            this.LbVreme.Location = new System.Drawing.Point(492, 97);
            this.LbVreme.Name = "LbVreme";
            this.LbVreme.Size = new System.Drawing.Size(87, 67);
            this.LbVreme.TabIndex = 48;
            this.LbVreme.Text = "90";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(505, 52);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(59, 23);
            this.Label2.TabIndex = 47;
            this.Label2.Text = "Vreme";
            // 
            // Button11
            // 
            this.Button11.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button11.Location = new System.Drawing.Point(360, 247);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(118, 56);
            this.Button11.TabIndex = 46;
            this.Button11.Text = "Restartuj";
            this.Button11.UseVisualStyleBackColor = true;
            this.Button11.Click += new System.EventHandler(this.BtnRestart_Click);
            // 
            // BtnObrisi
            // 
            this.BtnObrisi.Enabled = false;
            this.BtnObrisi.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnObrisi.Location = new System.Drawing.Point(187, 247);
            this.BtnObrisi.Name = "BtnObrisi";
            this.BtnObrisi.Size = new System.Drawing.Size(118, 56);
            this.BtnObrisi.TabIndex = 45;
            this.BtnObrisi.Text = "Obrisi";
            this.BtnObrisi.UseVisualStyleBackColor = true;
            this.BtnObrisi.Click += new System.EventHandler(this.BtnObrisi_Click);
            // 
            // BtnZagrada1
            // 
            this.BtnZagrada1.Enabled = false;
            this.BtnZagrada1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnZagrada1.Location = new System.Drawing.Point(326, 133);
            this.BtnZagrada1.Name = "BtnZagrada1";
            this.BtnZagrada1.Size = new System.Drawing.Size(72, 56);
            this.BtnZagrada1.TabIndex = 44;
            this.BtnZagrada1.Text = "(";
            this.BtnZagrada1.UseVisualStyleBackColor = true;
            this.BtnZagrada1.Click += new System.EventHandler(this.BtnZagrada1_Click);
            // 
            // BtnZagrada2
            // 
            this.BtnZagrada2.Enabled = false;
            this.BtnZagrada2.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnZagrada2.Location = new System.Drawing.Point(404, 134);
            this.BtnZagrada2.Name = "BtnZagrada2";
            this.BtnZagrada2.Size = new System.Drawing.Size(72, 56);
            this.BtnZagrada2.TabIndex = 43;
            this.BtnZagrada2.Text = ")";
            this.BtnZagrada2.UseVisualStyleBackColor = true;
            this.BtnZagrada2.Click += new System.EventHandler(this.Zagrada2_Click);
            // 
            // BtnMinus
            // 
            this.BtnMinus.Enabled = false;
            this.BtnMinus.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMinus.Location = new System.Drawing.Point(92, 133);
            this.BtnMinus.Name = "BtnMinus";
            this.BtnMinus.Size = new System.Drawing.Size(72, 56);
            this.BtnMinus.TabIndex = 42;
            this.BtnMinus.Text = "-";
            this.BtnMinus.UseVisualStyleBackColor = true;
            this.BtnMinus.Click += new System.EventHandler(this.BtnMinus_Click);
            // 
            // BtnPuta
            // 
            this.BtnPuta.Enabled = false;
            this.BtnPuta.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPuta.Location = new System.Drawing.Point(170, 134);
            this.BtnPuta.Name = "BtnPuta";
            this.BtnPuta.Size = new System.Drawing.Size(72, 56);
            this.BtnPuta.TabIndex = 41;
            this.BtnPuta.Text = "*";
            this.BtnPuta.UseVisualStyleBackColor = true;
            this.BtnPuta.Click += new System.EventHandler(this.BtnPuta_Click);
            // 
            // BtnPodeljeno
            // 
            this.BtnPodeljeno.Enabled = false;
            this.BtnPodeljeno.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPodeljeno.Location = new System.Drawing.Point(248, 133);
            this.BtnPodeljeno.Name = "BtnPodeljeno";
            this.BtnPodeljeno.Size = new System.Drawing.Size(72, 56);
            this.BtnPodeljeno.TabIndex = 40;
            this.BtnPodeljeno.Text = "/";
            this.BtnPodeljeno.UseVisualStyleBackColor = true;
            this.BtnPodeljeno.Click += new System.EventHandler(this.BtnPodeljeno_Click);
            // 
            // BtnPlus
            // 
            this.BtnPlus.Enabled = false;
            this.BtnPlus.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlus.Location = new System.Drawing.Point(14, 133);
            this.BtnPlus.Name = "BtnPlus";
            this.BtnPlus.Size = new System.Drawing.Size(72, 56);
            this.BtnPlus.TabIndex = 39;
            this.BtnPlus.Text = "+";
            this.BtnPlus.UseVisualStyleBackColor = true;
            this.BtnPlus.Click += new System.EventHandler(this.BtnPlus_Click);
            // 
            // LbResenje
            // 
            this.LbResenje.AutoSize = true;
            this.LbResenje.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbResenje.Location = new System.Drawing.Point(7, 328);
            this.LbResenje.Name = "LbResenje";
            this.LbResenje.Size = new System.Drawing.Size(193, 27);
            this.LbResenje.TabIndex = 38;
            this.LbResenje.Text = "Resavanje u toku...";
            // 
            // TextBox1
            // 
            this.TextBox1.BackColor = System.Drawing.Color.White;
            this.TextBox1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox1.Location = new System.Drawing.Point(14, 196);
            this.TextBox1.Multiline = true;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.ReadOnly = true;
            this.TextBox1.Size = new System.Drawing.Size(464, 45);
            this.TextBox1.TabIndex = 37;
            this.TextBox1.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // BtnStop
            // 
            this.BtnStop.Enabled = false;
            this.BtnStop.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStop.Location = new System.Drawing.Point(347, 35);
            this.BtnStop.Name = "BtnStop";
            this.BtnStop.Size = new System.Drawing.Size(129, 56);
            this.BtnStop.TabIndex = 36;
            this.BtnStop.Text = "Stop";
            this.BtnStop.UseVisualStyleBackColor = true;
            this.BtnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // BtnStart
            // 
            this.BtnStart.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStart.Location = new System.Drawing.Point(12, 35);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(127, 56);
            this.BtnStart.TabIndex = 35;
            this.BtnStart.Text = "Start";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // TBSest
            // 
            this.TBSest.BackColor = System.Drawing.Color.White;
            this.TBSest.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBSest.Location = new System.Drawing.Point(404, 97);
            this.TBSest.Name = "TBSest";
            this.TBSest.ReadOnly = true;
            this.TBSest.Size = new System.Drawing.Size(72, 30);
            this.TBSest.TabIndex = 34;
            this.TBSest.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TBSest.Click += new System.EventHandler(this.TBSest_Click);
            // 
            // TBPet
            // 
            this.TBPet.BackColor = System.Drawing.Color.White;
            this.TBPet.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBPet.Location = new System.Drawing.Point(326, 97);
            this.TBPet.Name = "TBPet";
            this.TBPet.ReadOnly = true;
            this.TBPet.Size = new System.Drawing.Size(72, 30);
            this.TBPet.TabIndex = 33;
            this.TBPet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TBPet.Click += new System.EventHandler(this.TBPet_Click);
            // 
            // TBDva
            // 
            this.TBDva.BackColor = System.Drawing.Color.White;
            this.TBDva.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBDva.Location = new System.Drawing.Point(92, 97);
            this.TBDva.Name = "TBDva";
            this.TBDva.ReadOnly = true;
            this.TBDva.Size = new System.Drawing.Size(72, 30);
            this.TBDva.TabIndex = 32;
            this.TBDva.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TBDva.Click += new System.EventHandler(this.TBDva_Click);
            // 
            // TBTri
            // 
            this.TBTri.BackColor = System.Drawing.Color.White;
            this.TBTri.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBTri.Location = new System.Drawing.Point(170, 98);
            this.TBTri.Name = "TBTri";
            this.TBTri.ReadOnly = true;
            this.TBTri.Size = new System.Drawing.Size(72, 30);
            this.TBTri.TabIndex = 31;
            this.TBTri.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TBTri.Click += new System.EventHandler(this.TBTri_Click);
            // 
            // TBCetiri
            // 
            this.TBCetiri.BackColor = System.Drawing.Color.White;
            this.TBCetiri.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBCetiri.Location = new System.Drawing.Point(248, 97);
            this.TBCetiri.Name = "TBCetiri";
            this.TBCetiri.ReadOnly = true;
            this.TBCetiri.Size = new System.Drawing.Size(72, 30);
            this.TBCetiri.TabIndex = 30;
            this.TBCetiri.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TBCetiri.Click += new System.EventHandler(this.TBCetiri_Click);
            // 
            // TBJedan
            // 
            this.TBJedan.BackColor = System.Drawing.Color.White;
            this.TBJedan.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBJedan.Location = new System.Drawing.Point(12, 97);
            this.TBJedan.Name = "TBJedan";
            this.TBJedan.ReadOnly = true;
            this.TBJedan.Size = new System.Drawing.Size(72, 30);
            this.TBJedan.TabIndex = 29;
            this.TBJedan.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TBJedan.Click += new System.EventHandler(this.TBJedan_Click);
            
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(198, 35);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(107, 23);
            this.Label1.TabIndex = 28;
            this.Label1.Text = "Trazeni broj";
            // 
            // TBTrazeni
            // 
            this.TBTrazeni.BackColor = System.Drawing.Color.White;
            this.TBTrazeni.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBTrazeni.Location = new System.Drawing.Point(175, 61);
            this.TBTrazeni.Name = "TBTrazeni";
            this.TBTrazeni.ReadOnly = true;
            this.TBTrazeni.Size = new System.Drawing.Size(137, 30);
            this.TBTrazeni.TabIndex = 27;
            this.TBTrazeni.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BtnPotvrdi
            // 
            this.BtnPotvrdi.Enabled = false;
            this.BtnPotvrdi.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPotvrdi.Location = new System.Drawing.Point(14, 247);
            this.BtnPotvrdi.Name = "BtnPotvrdi";
            this.BtnPotvrdi.Size = new System.Drawing.Size(118, 56);
            this.BtnPotvrdi.TabIndex = 26;
            this.BtnPotvrdi.Text = "Potvrdi";
            this.BtnPotvrdi.UseVisualStyleBackColor = true;
            this.BtnPotvrdi.Click += new System.EventHandler(this.BtnPotvrdi_Click);
            // 
            // TimerRandom
            // 
            this.TimerRandom.Tick += new System.EventHandler(this.TimerRandom_Tick);
            // 
            // TimerVreme
            // 
            this.TimerVreme.Tick += new System.EventHandler(this.TimerVreme_Tick);
            // 
            // TimerOdbrojavanje
            // 
            this.TimerOdbrojavanje.Interval = 1000;
            this.TimerOdbrojavanje.Tick += new System.EventHandler(this.TimerOdbrojavanje_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Moj broj";
            this.notifyIcon1.Visible = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restartujToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(573, 24);
            this.menuStrip1.TabIndex = 52;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // restartujToolStripMenuItem
            // 
            this.restartujToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaIgraToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.restartujToolStripMenuItem.Name = "restartujToolStripMenuItem";
            this.restartujToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.restartujToolStripMenuItem.Text = "File";
            // 
            // novaIgraToolStripMenuItem
            // 
            this.novaIgraToolStripMenuItem.Name = "novaIgraToolStripMenuItem";
            this.novaIgraToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.novaIgraToolStripMenuItem.Text = "Nova igra";
            this.novaIgraToolStripMenuItem.Click += new System.EventHandler(this.novaIgraToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click_1);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutMojBorojToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutMojBorojToolStripMenuItem
            // 
            this.aboutMojBorojToolStripMenuItem.Name = "aboutMojBorojToolStripMenuItem";
            this.aboutMojBorojToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.aboutMojBorojToolStripMenuItem.Text = "About Moj Broj";
            this.aboutMojBorojToolStripMenuItem.Click += new System.EventHandler(this.aboutMojBorojToolStripMenuItem_Click_1);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(435, 201);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(38, 22);
            this.button12.TabIndex = 53;
            this.button12.Text = "<<";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 357);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.LbDigitron);
            this.Controls.Add(this.LbVreme);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Button11);
            this.Controls.Add(this.BtnObrisi);
            this.Controls.Add(this.BtnZagrada1);
            this.Controls.Add(this.BtnZagrada2);
            this.Controls.Add(this.BtnMinus);
            this.Controls.Add(this.BtnPuta);
            this.Controls.Add(this.BtnPodeljeno);
            this.Controls.Add(this.BtnPlus);
            this.Controls.Add(this.LbResenje);
            this.Controls.Add(this.TextBox1);
            this.Controls.Add(this.BtnStop);
            this.Controls.Add(this.BtnStart);
            this.Controls.Add(this.TBSest);
            this.Controls.Add(this.TBPet);
            this.Controls.Add(this.TBDva);
            this.Controls.Add(this.TBTri);
            this.Controls.Add(this.TBCetiri);
            this.Controls.Add(this.TBJedan);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.TBTrazeni);
            this.Controls.Add(this.BtnPotvrdi);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(612, 416);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Moj Broj";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label LbDigitron;
        internal System.Windows.Forms.Label LbVreme;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button Button11;
        internal System.Windows.Forms.Button BtnObrisi;
        internal System.Windows.Forms.Button BtnZagrada1;
        internal System.Windows.Forms.Button BtnZagrada2;
        internal System.Windows.Forms.Button BtnMinus;
        internal System.Windows.Forms.Button BtnPuta;
        internal System.Windows.Forms.Button BtnPodeljeno;
        internal System.Windows.Forms.Button BtnPlus;
        internal System.Windows.Forms.Label LbResenje;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.Button BtnStop;
        internal System.Windows.Forms.Button BtnStart;
        internal System.Windows.Forms.TextBox TBSest;
        internal System.Windows.Forms.TextBox TBPet;
        internal System.Windows.Forms.TextBox TBDva;
        internal System.Windows.Forms.TextBox TBTri;
        internal System.Windows.Forms.TextBox TBCetiri;
        internal System.Windows.Forms.TextBox TBJedan;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox TBTrazeni;
        internal System.Windows.Forms.Button BtnPotvrdi;
        internal System.Windows.Forms.Timer TimerRandom;
        internal System.Windows.Forms.Timer TimerVreme;
        internal System.Windows.Forms.Timer TimerOdbrojavanje;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem restartujToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutMojBorojToolStripMenuItem;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.ToolStripMenuItem novaIgraToolStripMenuItem;
    }
}

